﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using AssemblyCSharp;
using System.Linq;

public class AbilitySpawner : MonoBehaviour {

	[Header("Prefabs")]
	public PickupController PickupPrefab;

	[Header("Spawn Points")]
	public Transform[] SpawnPoints;
	public List<AbilitySpawnChance> AbilitySpawnChances = new List<AbilitySpawnChance> ();

	[Header("Timings")]
	public float SpawnTime = 4f;
	public RangedFloat SpawnRandomness;

	// Use this for initialization
	void Start () {
		var spawnTime = SpawnTime * SpawnRandomness.random;

		StartCoroutine (SpawnCoroutine (1, false));
		StartCoroutine (SpawnCoroutine (2, false));
		StartCoroutine (SpawnCoroutine (3, false));

		StartCoroutine (SpawnCoroutine (spawnTime));
	}

	private IEnumerator SpawnCoroutine(float time, bool reset = true) {

		yield return new WaitForSeconds (time);
	
		Spawn ();

		if (reset) {
			var spawnTime = SpawnTime * SpawnRandomness.random;
			StartCoroutine (SpawnCoroutine (spawnTime));
		}
	}
		
	public void Spawn() {
		
		var totalChance = AbilitySpawnChances.Sum (x => x.Chance);

		var chance = UnityEngine.Random.Range (0f, totalChance);

		var index = 0;
		var abilityChance = AbilitySpawnChances [index];
		chance -= abilityChance.Chance;

		while (chance > 0f) {
			index++;
			abilityChance = AbilitySpawnChances [index];
			chance -= abilityChance.Chance;
		}

		var ability = abilityChance.Ability;

		var pickup = Instantiate (PickupPrefab, SpawnPoints [UnityEngine.Random.Range (0, SpawnPoints.Length)].position, Quaternion.identity);
		pickup.Ability = ability;
		pickup.GetComponent<Rigidbody2D> ().AddForce (Vector2.down * 500 + Vector2.right * UnityEngine.Random.Range(-500, 500));
	}

	[Serializable]
	public class AbilitySpawnChance {
		public AmoebaPickup Ability;
		public float Chance;
	}
}
