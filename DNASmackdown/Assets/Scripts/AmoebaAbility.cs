﻿using System;
using UnityEngine;
using DG.Tweening;
using UnityStandardAssets._2D;

namespace AssemblyCSharp
{
	[CreateAssetMenu(menuName="Amoeba Ability")]
	public class AmoebaAbility : AmoebaPickup
	{
		[Header("Prefab")]
		public ProjectileController AbilityPrefab;
		public bool ShouldParent = false;

		[Header("Speed and Direction")]
		public float Speed = 5f;
		public bool FromPlayerDirection = true;

		[Header("Duration")]
		public float Duration = 3f;

		public void Use(AmoebaController controller) {

			var ability = Instantiate (AbilityPrefab, controller.ShootPoint.position, Quaternion.identity);
			ability.gameObject.layer = LayerMask.NameToLayer (controller.BulletLayer);

			if (ShouldParent) {
				ability.transform.SetParent (controller.transform);
			}

			ability.Owner = controller;

			if (FromPlayerDirection) {
				ability.transform.DOMove (((Vector2)controller.transform.position)
					+ (controller.GetComponent<PlatformerCharacter2D>().Facing * Speed * Duration), Duration, false);
			}

			Destroy (ability.gameObject, Duration);

		}

		public override bool Pickup (AmoebaController controller)
		{
			if (controller.CurrentAbility == null) {

				controller.CurrentAbility = this;
				return true;
			}
			return false;
		}
	}
}

