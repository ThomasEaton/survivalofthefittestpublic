﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using RSG;
using AssemblyCSharp;

public class AmoebaController : MonoBehaviour {

	public int PlayerNumber = 0;

	[SerializeField]
	private int _health = 6;
	public int Health { get { return _health; } set { _health = Mathf.Clamp (value, 0, MaxHealth); } }
	public int MaxHealth = 6;

	public float InvinciblityFrameTime = 1f;
	public bool IsInvincible = false;

	public AmoebaAbility CurrentAbility;

	public Collider2D HitCollider;

	public Transform ShootPoint;

	public Sprite DeadSprite;

	List<AmoebaEffect> EffectDictionary = new List<AmoebaEffect>();

	public string FireButton = "Fire1";
	public string BulletLayer = "Player1Bullet";

	public bool IsBigulated;
	private bool isBig;

	public float MushroomFactor = 1.75f;

	[Header("Audio")]
	public AudioSource EffectSource;
	public AudioClip HitSound;


	// Update is called once per frame
	void Update () {

		// Use Ability.
		if (Input.GetButtonDown (FireButton) && CurrentAbility != null) {

			CurrentAbility.Use (this);
			CurrentAbility = null;
		}

		if (IsBigulated && !isBig)
		{
			// Need to get big.
			transform.localScale *= MushroomFactor;
			isBig = true;
		}

		if (!IsBigulated && isBig)
		{
			// Need to get small.
			transform.localScale /= MushroomFactor;
			isBig = false;
		}
	}

	public void OnCollisionEnter2D(Collision2D collision) {


		// If there is another player that colliders with only the hit collider, bounce that object and take damage.
		var otherPlayer = collision.collider.gameObject.GetComponent<AmoebaController> ();

		if (collision.otherCollider == HitCollider && otherPlayer != null) {

			otherPlayer.GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 600);
			Hit ();
		}
	}

	public void Hit(int damage = 1) {

		if (!IsInvincible) {
			
			Debug.Log ("Hit!");
			StartCoroutine (SetInvincible());

			Health -= damage;

			EffectSource.PlayOneShot (HitSound);

			if (Health == 0) {
				Die ();
			}
		}
	}

	public void Die() {

		GetComponent<AmoebaFacade> ().AmoebaController.enabled = false;
		GetComponent<AmoebaFacade> ().PlatformerController.enabled = false;

		GetComponent<SpriteRenderer> ().sprite = DeadSprite;

		GameObject.FindObjectOfType<VictoryController> ().Victory ((PlayerNumber + 1) % 2);

	}

	public bool ApplyEffect(AmoebaEffect effect) {

		if (!EffectDictionary.Contains(effect)) {
			StartCoroutine (ApplyEffectCoroutine (effect));
			return true;
		}

		return false;
	}

	private IEnumerator ApplyEffectCoroutine(AmoebaEffect effect) {
		effect.OnActivate (this);
		EffectDictionary.Add (effect);
		yield return new WaitForSeconds (effect.Duration);
		effect.OnDeactivate (this);
		EffectDictionary.Remove (effect);
	}

	private IEnumerator SetInvincible() {

		IsInvincible = true;
		yield return new WaitForSeconds (InvinciblityFrameTime);
		IsInvincible = false;
	}


}
