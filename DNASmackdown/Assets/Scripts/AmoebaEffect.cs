﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	[CreateAssetMenu(menuName="AmoebaEffect")]
	public class AmoebaEffect : ScriptableObject
	{
		public float Duration = 3f;

		public int HealthModifier = 0;
		public int JumpModifier = 0;
	    public int SpeedModifier = 0;

	    public bool ActivateInvicibility;

	    public bool CauseConfusion;

        public bool CauseBigulation;

		public GameObject EffectSystem;

		public void OnActivate(AmoebaController controller) {
			controller.Health += HealthModifier;

			// Apply each of the effects if possible.
			// TODO: Move each of these to the Delegate Pattern instead of a checklist of conditions.
		    var facade = controller.GetComponent<AmoebaFacade>();
		    facade.JumpForce += JumpModifier;
		    if (JumpModifier > 0)
		    {
		        facade.IsJumpBoostEnabled = true;
		    }
		    facade.MaxSpeed += SpeedModifier;

		    if (ActivateInvicibility)
		    {
		        facade.IsInvincible = true;
		    }

		    if (CauseConfusion)
		    {
		        facade.IsConfused = true;
		    }

		    if (CauseBigulation)
		    {
		        facade.IsBigulated = true;
		    }

			// Play an effect if there is one.
            if (EffectSystem != null)
            {
                Debug.Log("creating effect:" + EffectSystem.name);
				var instance = Instantiate (EffectSystem, controller.transform.position, Quaternion.identity);
				instance.transform.SetParent (controller.transform);

				Destroy (instance.gameObject, Duration);
			}
		}

		public void OnDeactivate(AmoebaController controller) {

			// Deactive all of the effects.
			// TODO: This should be an abstract object going forward to prevent the checklist of conditions.
		    var facade = controller.GetComponent<AmoebaFacade>();
		    facade.JumpForce -= JumpModifier;
		    if (JumpModifier > 0)
		    {
		        facade.IsJumpBoostEnabled = false;
		    }
		    facade.MaxSpeed -= SpeedModifier;

		    if (ActivateInvicibility)
		    {
		        facade.IsInvincible = false;
		    }

		    if (CauseConfusion)
		    {
		        facade.IsConfused = false;
		    }

		    if (CauseBigulation)
		    {
		        facade.IsBigulated = false;
		    }
        }
    }
}

