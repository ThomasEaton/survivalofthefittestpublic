﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	[CreateAssetMenu(menuName="AmoebaEffectPickup")]
	public class AmoebaEffectPickup : AmoebaPickup
	{
		public AmoebaEffect EffectToApply;

		public override bool Pickup (AmoebaController controller)
		{
			if (controller.ApplyEffect (EffectToApply)) {
				return true;
			}

			return false;
		}
	}
}

