﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class AmoebaFacade : MonoBehaviour {

	public AmoebaController AmoebaController;
	public PlatformerCharacter2D PlatformerController;

	public int Health { get { return AmoebaController.Health; } set { AmoebaController.Health = value; } }
	public int MaxHealth  { get { return AmoebaController.MaxHealth; } set { AmoebaController.MaxHealth = value; } }

	public float MaxSpeed  { get { return PlatformerController.m_MaxSpeed; } set { PlatformerController.m_MaxSpeed = value; } }
	public float JumpForce  { get { return PlatformerController.m_JumpForce; } set { PlatformerController.m_JumpForce = value; } }

    public bool IsInvincible { get { return AmoebaController.IsInvincible; }  set { AmoebaController.IsInvincible = value; } }
    public bool IsJumpBoostEnabled { get { return PlatformerController.IsJumpBoostEnabled; }  set { PlatformerController.IsJumpBoostEnabled = value; } }
    public bool IsConfused { get { return PlatformerController.IsConfused; }  set { PlatformerController.IsConfused = value; } }
    public bool IsBigulated { get { return AmoebaController.IsBigulated; } set { AmoebaController.IsBigulated = value; } }
}
