﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public abstract class AmoebaPickup : ScriptableObject
	{
		public string Name;
		public Color Color;

		public Sprite Icon;

		public abstract bool Pickup(AmoebaController controller);	
	}
}

