﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class CreditsController : MonoBehaviour {

	public CreditsScreen[] CreditsScreens;
	public string SceneToLoad = "SceneName";

	private int _currentIndex = 0;
	private float _currentTime = 0f;

	void Start () {

		foreach (var screen in CreditsScreens) {
			screen.SceneToActivate.SetActive (false);
		}

		CreditsScreens[0].SceneToActivate.SetActive (true);
	}

	void Update () {

		_currentTime += Time.deltaTime;

		// Play through each of the scenes up to the last one.
		if (_currentIndex != CreditsScreens.Length) {

			if (_currentTime >= CreditsScreens [_currentIndex].Time) {
				_currentIndex++;
			
				// Go to next scene.
				if (_currentIndex == CreditsScreens.Length) {
					SceneManager.LoadScene (SceneToLoad);
					this.enabled = false;
				} else {

					// At the end, go to the next scene.
					foreach (var screen in CreditsScreens) {
						screen.SceneToActivate.SetActive (false);
					}

					CreditsScreens[_currentIndex].SceneToActivate.SetActive (true);
				}
			}

		}

		// Skip to the end.
		if (Input.anyKey) {
			SceneManager.LoadScene (SceneToLoad);
		}
	}

	[Serializable]
	public class CreditsScreen {
		public float Time;
		public GameObject SceneToActivate;
	}
}
