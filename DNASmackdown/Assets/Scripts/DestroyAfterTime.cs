﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {

	public float Time = 3f;


	// Use this for initialization
	void Start () {
		StartCoroutine (DestroySelf ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public IEnumerator DestroySelf() {
		yield return new WaitForSeconds (Time);
		Destroy (this.gameObject);
	}
}
