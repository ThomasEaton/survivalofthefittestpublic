﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToSceneController : MonoBehaviour {

	public string SceneName = "SceneName";

	public void GoToScene() {
		SceneManager.LoadScene(SceneName);
	}
}
