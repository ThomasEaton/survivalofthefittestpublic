﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveVelocityController : MonoBehaviour {

	public Vector2 Distance;
	public float Time;

	// Use this for initialization
	void Start () {

		this.transform.DOMove ((Vector2)this.transform.position + Distance, Time);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
