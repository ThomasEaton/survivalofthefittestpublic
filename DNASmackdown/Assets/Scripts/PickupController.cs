﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;
using TMPro;

public class PickupController : MonoBehaviour {

	public AmoebaPickup Ability;
	public SpriteRenderer IconRenderer;
	public AudioClip PickupSound;

	public TextMeshPro HoverText;

	// Use this for initialization
	void Start () {
		IconRenderer.sprite = Ability.Icon;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter2D(Collider2D collider) {

		// Check to see if the collider was an Amoeba Controller.
		var other = collider.GetComponent<AmoebaController> ();

		if (other != null) {

			// Check to see if you are able to pick this item up.
			if (Ability.Pickup (other)) {

				// Display hover text.
				var text = Instantiate (HoverText, this.transform.position, Quaternion.identity);
				text.text = Ability.Name;
				text.color = Ability.Color;

				// Play the pickup sound.
				other.EffectSource.PlayOneShot (PickupSound);

				Destroy (this.gameObject);
			}


		}

	}
}
