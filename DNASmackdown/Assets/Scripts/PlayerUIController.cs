﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIController : MonoBehaviour {

	public AmoebaController Amoeba;

	[Header("Icons")]
	public Image AbilityImage;
	public Image[] HealthIcon;

	// Update is called once per frame
	void Update () {

		if (Amoeba == null) {
			return;
		}

		// Display the Current Ability only if you have an ability to display.
		if (Amoeba.CurrentAbility != null) {
			AbilityImage.gameObject.SetActive (true);
			AbilityImage.sprite = Amoeba.CurrentAbility.Icon;
		} else {
			AbilityImage.gameObject.SetActive (false);
		}

		// Display the Hearts.
		for (var i = 0; i < HealthIcon.Length; i++) {
			if (i < Amoeba.Health) {
				HealthIcon [i].gameObject.SetActive (true);
			} else {
				HealthIcon [i].gameObject.SetActive (false);
			}
		}


	}
}
