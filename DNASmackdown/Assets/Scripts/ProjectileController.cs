﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;

public class ProjectileController : MonoBehaviour {

	[HideInInspector] public AmoebaController Owner;

	public int Damage = 1;
	public bool DestroyOnHit = false;
	public AmoebaEffect EffectToApply;
    public int RotationsPerMinute;

	private float _timeBeforeCollision = 0.04f;

	// Update is called once per frame
	void Update () {
	   
		_timeBeforeCollision -= Time.deltaTime;

		if (RotationsPerMinute != 0)
	    {
	        transform.Rotate(0,  0, 6.0f * RotationsPerMinute * Time.deltaTime);
        }
	}

	// Uses CollisionStay2D instead of CollisionEnter2D because of the time before collision check.
	public void OnCollisionStay2D(Collision2D other) {

		/* There should be a brief gap between when the projectile is shot and when the bullet can collider with another object */
		if (_timeBeforeCollision > 0f)
			return;

		// Make sure you can't collide with yourself.
		if (other.gameObject == Owner.gameObject)
			return;


		// If you hit another player, deal damage to it.
		var otherPlayer = other.gameObject.GetComponent<AmoebaController> ();

		if (otherPlayer != null) {

			// Deal damage
			otherPlayer.Hit (Damage);

			// Apply any on hit effects if the player allows it.
			if (EffectToApply != null) {
				otherPlayer.ApplyEffect (EffectToApply);
			}

			if (DestroyOnHit) {
				Destroy (this.gameObject);
			}
		}
	}
}
