﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName="SoundContainer")]
public class SoundContainer : ScriptableObject
{
	public AudioClip[] Clips;

	public void Play(AudioSource source) {

		// Play a random audio clip to keep things interesting.
		var clip = Clips [UnityEngine.Random.Range (0, Clips.Length)];

		source.PlayOneShot (clip);
	}
}
