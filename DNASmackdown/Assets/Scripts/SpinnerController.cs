﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerController : MonoBehaviour {

	public float SpinSpeed = 3;

	void Update () {
		transform.Rotate (Vector3.forward, Time.deltaTime * SpinSpeed);
	}
}
