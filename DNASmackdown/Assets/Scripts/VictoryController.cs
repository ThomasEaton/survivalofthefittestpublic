﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class VictoryController : MonoBehaviour {

	public TMPro.TextMeshPro[] PlayerVictoryText;
	public Transform VictoryTextPoint;

	public string TitleScene = "TitleScreen";

	private bool _isVictoryPlaying = false;
		
	public void Victory(int player) {

		if (_isVictoryPlaying) {
			return;
		}

		_isVictoryPlaying = true;

		PlayerVictoryText [player].transform.DOMove (VictoryTextPoint.transform.position, 1.5f);

		StartCoroutine (ReturnToTitleScreen (4));
	}

	private IEnumerator ReturnToTitleScreen(float time) {

		yield return new WaitForSeconds (time);
		SceneManager.LoadScene (TitleScene);
	}
}
