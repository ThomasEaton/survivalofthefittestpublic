﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(RangedFloat), true)]
public class RangedFloatDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		label = EditorGUI.BeginProperty(position, label, property);
		position = EditorGUI.PrefixLabel(position, label);

		SerializedProperty minProp = property.FindPropertyRelative("minValue");
		SerializedProperty maxProp = property.FindPropertyRelative("maxValue");

		float minValue = minProp.floatValue;
		float maxValue = maxProp.floatValue;

		float rangeMin = 0;
		float rangeMax = 1;

		var ranges = (MinMaxRangeAttribute[])fieldInfo.GetCustomAttributes(typeof(MinMaxRangeAttribute), true);
		if (ranges.Length > 0)
		{
			rangeMin = ranges[0].Min;
			rangeMax = ranges[0].Max;
		}

		const float rangeBoundsLabelWidth = 45f;

		GUI.color=Color.cyan;

		var resetButton = new Rect(position);
		resetButton.width = 20;
		var btn = GUI.Button(resetButton, "R",EditorStyles.miniButton);//RESET BUTTON
		GUI.color = Color.white;

		position.xMin += resetButton.width+4;
		var rangeBoundsLabel1Rect = new Rect(position);
		rangeBoundsLabel1Rect.width = rangeBoundsLabelWidth;



		#region Extra Min inputs

		//GUI.Label(rangeBoundsLabel1Rect, new GUIContent(minValue.ToString("F2")));//orig

		var smin = minValue.ToString("F2");

		var min = GUI.TextField(rangeBoundsLabel1Rect, smin);

		position.xMin += 4 + rangeBoundsLabelWidth;

		float flt;
		if (float.TryParse(min, out flt)) { minProp.floatValue = flt; }

		#endregion


		var rangeBoundsLabel2Rect = new Rect(position);
		rangeBoundsLabel2Rect.xMin = rangeBoundsLabel2Rect.xMax - rangeBoundsLabelWidth;



		#region Extra Max input and Reset Button

		//GUI.Label(rangeBoundsLabel2Rect, new GUIContent(maxValue.ToString("F2")));//orig

		string smax = maxValue.ToString("F2");
		string max = GUI.TextField(rangeBoundsLabel2Rect, smax);

		float flt2;
		if (float.TryParse(max, out flt2)) { maxProp.floatValue = flt2; }



		if (btn)
		{
			//RESET BUTTON ACTION to middle of ranges
			var middle = (rangeMax - Mathf.Abs(rangeMin))/2;
			minProp.floatValue = middle;
			maxProp.floatValue = middle;

		}

		#endregion

		position.xMax -= rangeBoundsLabelWidth;
		position.xMax -= 4;

		EditorGUI.BeginChangeCheck();

		EditorGUI.MinMaxSlider(position, ref minValue, ref maxValue, rangeMin, rangeMax);

		if (EditorGUI.EndChangeCheck())
		{
			minProp.floatValue = minValue;
			maxProp.floatValue = maxValue;
		}

		EditorGUI.EndProperty();
	}
}