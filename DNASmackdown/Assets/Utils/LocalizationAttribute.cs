﻿using System;

namespace Idify {

	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class LocalizationAttribute : Attribute { 

		private string _key;
		private string _defaultKey;

		public LocalizationAttribute(string key) {
			_key = key;
		}

		public string Key {
			get { return _key; }
		}

		public string DefaultKey {
			get { return _defaultKey; }
			set { _defaultKey = value; }
		}
	}

}

