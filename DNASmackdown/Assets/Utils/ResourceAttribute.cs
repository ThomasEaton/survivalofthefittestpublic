﻿using System;

[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
public class ResourceAttribute : Attribute
{
	private string _key;
	public ResourceAttribute(string key) {
		_key = key;
	}

	public string Key {
		get { return _key; }
	}
}

